#include <LiquidCrystal.h>

const int SUPPLY_SENSOR_PORT = 0;
const int RETURN_SENSOR_PORT = 1;

const int ACTIVITY_LED_PORT = LED_BUILTIN;
const int SAMPLE_RATE = 250;  // Every n milliseconds

class Thermometer {
  private:
    static constexpr double VOLTAGE_DIVIDER_RESISTOR = 10e3;
    static constexpr double ANALOG_PORT_SAMPLE_RESOLUTION = 1024;
    static constexpr double ANALOG_PORT_REFERENCE_VOLTAGE = 5;
    static constexpr double THERMISTOR_COEFFICIENT_A = 0.0009171760965928652;
    static constexpr double THERMISTOR_COEFFICIENT_B = 0.0002481307229745353;
    static constexpr double THERMISTOR_COEFFICIENT_C = 2.02685097359127e-7;
    static constexpr double ZERO_CELSIUS = 273.15;      
    static constexpr int BUFFER_SIZE = 10;
    
    double buffer[BUFFER_SIZE];
    int nextIndex = 0;
    int analogPort;

    double measure() {
      // Measure voltage
      int sample = analogRead(analogPort);
      double voltage = ANALOG_PORT_REFERENCE_VOLTAGE * sample / (ANALOG_PORT_SAMPLE_RESOLUTION - 1);

      // Measure resistance
      double resistance = (VOLTAGE_DIVIDER_RESISTOR * voltage) / (ANALOG_PORT_REFERENCE_VOLTAGE - voltage);

      // Measure temperature  
      double lnr = log(resistance);
      double temperature = (1 / (THERMISTOR_COEFFICIENT_A + THERMISTOR_COEFFICIENT_B * lnr  + THERMISTOR_COEFFICIENT_C * lnr * lnr * lnr)) - ZERO_CELSIUS;
  
      return temperature;
    }

    /**
     * Take a sample and store it in the buffer, discarding the oldest value.
     */
    void sample() {
      buffer[nextIndex++] = measure();
      if(nextIndex == BUFFER_SIZE) {
        nextIndex = 0;
      }
    }

  public:    
    Thermometer(int analogPort) {
      this->analogPort = analogPort;
    }

    /**
     * Takes a new temperature sample and return the average temperature within all buffered samples.
     */
    double temperature() {
      sample();
      
      double sum = 0;
      for(int i = 0; i < BUFFER_SIZE; i++) {
        sum += buffer[i];
      }
      return sum / BUFFER_SIZE;
    }
};

LiquidCrystal lcd(12, 11, 5, 4, 3, 2);

Thermometer supplyThermometer(SUPPLY_SENSOR_PORT);
Thermometer returnThermometer(RETURN_SENSOR_PORT);

void printTemperature(Thermometer *thermometer, String label, int line) {  
  lcd.setCursor(0, line);

  double temperature = thermometer->temperature();
  
  lcd.print(label);
  lcd.print(temperature, 1);  
  lcd.print((char)178); // '°'
  lcd.print("C");
}

void setup() {
  lcd.begin(24, 2);
  pinMode(ACTIVITY_LED_PORT, OUTPUT);

  
}

void loop() {  
  printTemperature(&supplyThermometer, "Supply: ", 0);
  printTemperature(&returnThermometer, "Return: ", 1);

  digitalWrite(ACTIVITY_LED_PORT, LOW);
  delay(SAMPLE_RATE);
  digitalWrite(ACTIVITY_LED_PORT, HIGH);
}


